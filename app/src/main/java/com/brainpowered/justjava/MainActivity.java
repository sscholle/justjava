package com.brainpowered.justjava;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.text.NumberFormat;


public class MainActivity extends ActionBarActivity {
    private static final String TAG = "MainActivity";

    int cupsOfCoffee = 0;
//    boolean whippedCream = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); // refers to the xml layout resource
    }

    public void setWhippedCream(View view) {
        boolean isChecked = ((CheckBox) view).isChecked();
        //whippedCream = isChecked;
    }

    // left off at 3B - 06 (going back to 3A)
    // generic on checkboxclicked callback
    public void onCheckboxClicked(View view) {
        // get checked state
        boolean isChecked = ((CheckBox) view).isChecked();
        // switch based on view id
        switch (view.getId()) {
            case R.id.whippedCream:
                //whippedCream = isChecked;
        }

    }

    public void increaseQuantity(View view) {
        displayQuantity(++cupsOfCoffee);
    }

    public void decreaseQuantity(View view) {
        cupsOfCoffee = Math.max(0, cupsOfCoffee - 1);
        displayQuantity(cupsOfCoffee);

    }

    private void displayQuantity(int number) {
        Log.v(TAG, "Quantity is: "+number);
        TextView order_number = (TextView) findViewById(R.id.quantity_text_view);
        order_number.setText("" + number);
    }

    /**
     * Called when the order button is pressed
     * @param view
     */
    public void submitOrder(View view) {
        int pricePerCupOfCoffee = 10; // should get these dynaic vars from the config/app
        int whippedCreamPrice = 2;

        EditText textView = (EditText) findViewById(R.id.name_for_order);
        String name = textView.getText().toString();

        CheckBox whippedCream = (CheckBox) findViewById(R.id.whippedCream);
        boolean withWhippedCream = whippedCream.isChecked();



        int price = calculatePrice(cupsOfCoffee, withWhippedCream, pricePerCupOfCoffee, whippedCreamPrice);
        displayOrderSummary(name, price);
    }

    public int calculatePrice(int cupsOfCoffee, boolean whippedCream, int pricePerCupOfCoffee, int whippedCreamPrice) {
        return (whippedCream == true) ? cupsOfCoffee * pricePerCupOfCoffee + cupsOfCoffee * whippedCreamPrice : cupsOfCoffee* pricePerCupOfCoffee;
    }

    /**
     * Display order summary
     */
    private void displayOrderSummary(String name, int price) {
        TextView order_number = (TextView) findViewById(R.id.order_summary);
        String orderSummary = "Name: " + name + "\n" +
                //"Quantity: " + cupsOfCoffee + "\n" +
                //"Whipped Cream: " + whippedCream + "\n" +
                "Total: " + NumberFormat.getCurrencyInstance().format(price) + "\n" +
                "Thank You!";
        order_number.setText(orderSummary);
    }

    /**
     * not even used
     * @param number
     */
    private void displayPrice(int number) {
        TextView priceTextView = (TextView) findViewById(R.id.quantity_text_view);
        priceTextView.setText("" + number);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
